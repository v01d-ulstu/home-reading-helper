const LOCAL_STORAGE_KEY_SIDEBAR_IS_OPEN = "SIDEBAR_IS_OPEN"


const bool_from_str_repr = string => string === "true"


if (bool_from_str_repr(localStorage[LOCAL_STORAGE_KEY_SIDEBAR_IS_OPEN])) {
    $('#sidebar').removeClass('active')
}

$('#sidebarCollapse').on('click', function () {
    $('#sidebar').toggleClass('active')
    localStorage[LOCAL_STORAGE_KEY_SIDEBAR_IS_OPEN] = !bool_from_str_repr(
        localStorage[LOCAL_STORAGE_KEY_SIDEBAR_IS_OPEN]
    )
})
