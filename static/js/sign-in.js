document.querySelector("#form_signin").onsubmit = async (event) => {
    event.preventDefault()

    const login = document.querySelector("#inp_login").value.trim()
    const password = document.querySelector("#inp_password").value.trim()
    const remember = document.querySelector("#inp_remember").checked

    const data = new FormData()
    data.set("login", login)
    data.set("password", password)
    data.set("remember", remember)

    const response = await fetch("/api/sign-in", {
        method: "POST",
        body: data,
    })

    switch (response.status) {
        case 400:
            makeToast('Отсутсвует логин или пароль.')
            return
        case 403:
            makeToast('Неверный пароль.')
            return
        case 404:
            makeToast('Пользователь не найден.')
            return
    }

    location.href = "/user-page"
}
