document.querySelector("#form_signup").onsubmit = async (event) => {
    event.preventDefault()

    const login = document.querySelector("#inp_login").value.trim()
    const password1 = document.querySelector("#inp_password1").value.trim()
    const password2 = document.querySelector("#inp_password2").value.trim()

    if (password1 !== password2) {
        makeToast("Пароли должны быть равны!")
        return
    }

    const data = new FormData()
    data.set("login", login)
    data.set("password", password1)

    const response = await fetch("/api/sign-up", {
        method: "POST",
        body: data,
    })

    switch (response.status) {
        case 400:
            makeToast('Invalid login or password.')
            return
        case 403:
            makeToast('Login is not unique.')
            return
    }

    location.href = "/user-page"
}
