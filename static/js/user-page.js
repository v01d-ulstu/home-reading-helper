const bool_to_symbol = (value) => value ? "✔" : "❌"


document.querySelector("#date_info").innerHTML = strftime("%d %b %Y, %a", new Date())


fetch("/api/user-info").then(async (res) => {
    if (res.status === 401) {
        location.href = "/sign-in"
    }

    const user_info = await res.json()
    document.querySelector("#content__title").innerHTML = `Hello, ${user_info.login}!`
    document.querySelector("#user__login").innerHTML = user_info.login
    document.querySelector("#user__login").innerHTML = user_info.login

    fetch("/api/get-homereadings").then(async (res) => {
        const hr_info = await res.json()
        const circle = Circles.create({
            id: 'circles-1',
            value: hr_info.length,
            maxValue: 4,
            radius: getWidth(),
            width: 5,
            text: Math.ceil,
            duration: 1500,
            colors: ['#128594', 'aquamarine']
        })

        window.onresize = (e) => {
            circle.updateRadius(getWidth())
        }

        const curr_symbols = hr_info.reduce((sum, current) => sum + current.symbols, 0)
        document.querySelector("#symbolsAll").style.width = ((curr_symbols / user_info.need_symbols) * 100) + '%'
        document.querySelector("#symbolsAll").style.width = curr_symbols
        document.querySelector("#numSymbols").innerText = `${curr_symbols}/${user_info.need_symbols}`


        hr_info.forEach((hr) => {
            hr.reading = bool_to_symbol(hr.reading)
            hr.translation = bool_to_symbol(hr.translation)
            hr.retelling = bool_to_symbol(hr.retelling)
            hr.phrases_or_words = bool_to_symbol(hr.phrases_or_words)
        })

        hr_info.sort((hr1, hr2) => new Date(hr1.date).getTime() - new Date(hr2.date).getTime())

        const labels = []
        const seria = []
        hr_info.forEach((hr) => {
            labels.push(hr.date)
            seria.push(hr.symbols)
        })

        new Chartist.Line('.ct-chart', {
            labels: labels,
            series: [seria]
        }, {
            plugins: [
                Chartist.plugins.ctPointLabels({
                    textAnchor: 'middle'
                })
            ],
            fullWidth: false,
        })

        $('#table').bootstrapTable({
            columns: [{
                field: 'id',
                visible: false
            }, {
                field: 'date',
                title: 'Date HR'
            }, {
                field: 'title',
                title: 'Title',
            }, {
                field: 'symbols',
                title: 'Symbols'
            }, {
                field: 'reading',
                title: 'Reading'
            }, {
                field: 'translation',
                title: 'Translation'
            }, {
                field: 'retelling',
                title: 'Retelling'
            }, {
                field: 'phrases_or_words',
                title: 'Collocations/Words'
            }, {
                field: 'operate',
                title: 'Item Operate',
                align: 'center',
                clickToSelect: false,
                events: {
                    'click .remove': (e, value, row) => {
                        const data = new FormData()
                        data.set("homereading_id", row.id)
                        fetch("/api/delete-homereading", {
                            method: "POST",
                            body: data,
                        }).then(async (res) => {
                            if (res.status !== 200) {
                                alert(await res.text())
                                return
                            }
                            location.reload()
                        })
                    }
                },
                formatter: () => `
                  <a class="remove" href="javascript:void(0)" title="Remove">
                  <i class="fa fa-trash"></i>
                  </a>
            `
            }],
            data: hr_info
        })

        $('.loading-wrap').hide()
    })
})

document.querySelector("#hr-activity-reg").onsubmit = async (event) => {
    event.preventDefault()

    const title = document.querySelector("#inputTitle").value
    const symbols = document.querySelector("#inputSymbols").value
    const translation = document.querySelector("#checkboxTranslation").checked
    const retelling = document.querySelector("#checkboxRetelling").checked
    const phrases_or_words = document.querySelector("#checkboxPhrases").checked
    const reading = document.querySelector("#checkboxReading").checked
    const date = document.querySelector("#datepicker").value


    if (parseInt(symbols).toString() !== symbols) {
        alert("Invalid count of symbols")
        return
    }

    const data = new FormData()
    data.set("title", title)
    data.set("symbols", symbols)
    data.set("reading", reading)
    data.set("retelling", retelling)
    data.set("date", date)
    data.set("phrases_or_words", phrases_or_words)
    data.set("translation", translation)

    const response = await fetch("/api/add-homereading", {
        method: "POST",
        body: data,
    })

    if (response.status !== 200) {
        alert(await response.text())
        return
    }

    location.reload()
}


$('#datepicker').bootstrapMaterialDatePicker({
    format: 'YYYY-MM-DD',
    minDate: '2021-01-01',
    lang: "ru",
    date: true,
    time: false,
    monthPicker: false,
    year: true,
    clearButton: false,
    cancelText: 'ОТМЕНА',
    weekStart: 1,
})


document.querySelector("#setNeedSymbols").onclick = async () => {
    const newNeedSymbols = document.querySelector("#newNeedSymbols").value

    const data = new FormData()
    data.set("need_symbols", newNeedSymbols)
    const res = await fetch("/api/set-need-symbols", {
        method: "POST",
        body: data,
    })

    if (res.status != 200) {
        makeToast(await res.text())
        return
    }

    location.reload()
}


function getWidth() {
    return (window.innerWidth <= 1450) ? 80 : 160
}
