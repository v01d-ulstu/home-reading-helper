import flask
import jinja2.exceptions

import config


views = flask.Blueprint("views", __name__)


@views.route("/")
def route_index():
    return flask.send_from_directory(config.STATIC_FOLDER, "index.html")


@views.route("/sp-push-worker-fb.js")
def route_push():
    return flask.send_from_directory(config.STATIC_FOLDER, "js/lib/sp-push-worker-fb.js")


@views.route("/<view>")
def route_view(view):  # pylint: disable=inconsistent-return-statements
    try:
        return flask.send_from_directory(config.STATIC_FOLDER, f"{view}.html")
    except jinja2.exceptions.TemplateNotFound:
        flask.abort(404)
