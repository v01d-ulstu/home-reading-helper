import string
import datetime

import flask
import flask_login
import mstranslator
import requests
import sqlalchemy.exc
import werkzeug.security

import config
import models


api = flask.Blueprint("api", __name__)
translator = mstranslator.Translator(config.MS_TRANSLATOR_KEY)


def str_field_is_true(field: str, acceptable=("1", "true")) -> bool:
    return field.strip().lower() in acceptable


@api.after_request
def set_crossdomain_headers(response):
    """This middleware provides cross-domain requests"""
    response.headers["Access-Control-Allow-Origin"] = "*"
    return response


@api.route("/check", methods=["POST"])
def api_check():
    """Computes text stats
    Args:
        text

    Errors:
        400: Empty text
    returns: JSON {
        "total": int,
        "words": int
    }
    """

    try:
        text = flask.request.form["text"]
    except KeyError:
        flask.abort(400)

    return flask.jsonify(
        {
            "total": sum(symbol.isalnum() for symbol in text),
            "words": len(text.split()),
        }
    )


@api.route("/translate", methods=["POST"])
def api_translate():
    """Translates the text
    Uses Yandex.Dict to translate a single word
    Uses mstranslate (MS Azure Cognitive Translator) to translate the sentence

    Args:
        text

    Errors:
        400: Empty text
        404: Definition not found
        500: Failed to connect to Yandex
    returns: JSON {
        "text": str,
        "transcription": str or null,
        "translation": str
    }
    """

    try:
        text = flask.request.form["text"].strip()
    except KeyError:
        flask.abort(400)
    if not text:
        flask.abort(400)
    if len(text) > config.MAX_TEXT_LENGTH:
        flask.abort(413)  # Payload Too Large

    # If any whitespace symbol is in text then count of words > 1
    if any(symbol in text for symbol in string.whitespace):
        translation = translator.translate(text, lang_to="ru")
        respone = {
            "text": text,
            "translation": translation,
            "transcription": None,
        }
    else:
        yandex_dict_request_data = {
            "text": text,
            "lang": "en-ru",
            "key": config.YANDEX_DICT_KEY,
        }

        try:
            with requests.post(config.YANDEX_DICT_API_URL, data=yandex_dict_request_data) as res:
                parsed_response = res.json()
        except requests.exceptions.ConnectionError:
            flask.abort(500)

        if not parsed_response["def"]:
            flask.abort(404)

        definition = parsed_response["def"][0]
        translation_variants = definition["tr"][: config.YANDEX_DICT_MAX_TRANSLATION_VARIANTS]
        respone = {
            "text": definition["text"],
            "transcription": definition.get("ts"),
            "translation": ", ".join(variant["text"] for variant in translation_variants),
        }

    return flask.jsonify(respone)


@api.route("/sign-up", methods=["POST"])
def api_signup():
    """Creates a new user

    Args:
        login
        password

    Errors:
        400: Bad Request
        403: login is not unique
    """

    try:
        login = flask.request.form["login"].strip()
        password = flask.request.form["password"].strip()
    except KeyError:
        flask.abort(400, "No login or password")

    if not login or not password:
        flask.abort(400, "Empty login or password")

    if not login.isalnum():
        flask.abort(400, "Login must contain letters and digits only")

    user = models.User(login=login, password=werkzeug.security.generate_password_hash(password))
    models.db.session.add(user)

    try:
        models.db.session.commit()
    except sqlalchemy.exc.IntegrityError:
        flask.abort(403, "Login is not unique")

    flask_login.login_user(user, remember=True)

    return "OK"


@api.route("/sign-in", methods=["POST"])
def api_singin():
    """Log in user

    Args:
        login
        password
        remember

    Errors:
        400: Bad Request
        403: Wrong password
        404: User not found
    """

    try:
        login = flask.request.form["login"].strip()
        password = flask.request.form["password"].strip()
        remember = str_field_is_true(flask.request.form["remember"])
    except KeyError:
        flask.abort(400, "No login or password")

    try:
        user = models.User.query.filter(models.User.login == login).one()
    except sqlalchemy.exc.NoResultFound:
        flask.abort(404, "User not found")

    if werkzeug.security.check_password_hash(user.password, password):
        flask_login.login_user(user, remember)
    else:
        flask.abort(403, "Wrong password")

    return "OK"


@api.route("/sign-out")
@flask_login.login_required
def api_signout():
    flask_login.logout_user()
    return "OK"


@api.route("/set-need-symbols", methods=["POST"])
@flask_login.login_required
def api_set_need_symbols():
    """
    Args:
        need_symbols: int

    Errors:
        400
    """

    try:
        need_symbols = int(flask.request.form["need_symbols"])
    except (KeyError, ValueError):
        flask.abort(400)

    flask_login.current_user.need_symbols = need_symbols
    models.db.session.commit()

    return "OK"


@api.route("/user-info")
@flask_login.login_required
def api_userinfo():
    return flask.jsonify(flask_login.current_user.get_public_data_as_dict())


@api.route("/add-homereading", methods=["POST"])
@flask_login.login_required
def api_add_homereading():
    """Add HR Results for current user

    Args:
        title: string
        symbols: int
        reading: bool
        retelling: bool
        translation: bool
        phrases_or_words: bool
        date: string, iso-string

    Errors:
        400:
            Not enough info
            Invalid iso-string
    """

    try:
        title = flask.request.form["title"].strip()
        symbols = int(flask.request.form["symbols"])
        reading = str_field_is_true(flask.request.form["reading"])
        retelling = str_field_is_true(flask.request.form["retelling"])
        translation = str_field_is_true(flask.request.form["translation"])
        phrases_or_words = str_field_is_true(flask.request.form["phrases_or_words"])
        iso_date = flask.request.form["date"].strip()
    except (KeyError, ValueError):
        flask.abort(400)

    if not title:
        flask.abort(400)

    try:
        date = datetime.date.fromisoformat(iso_date)
    except ValueError:
        flask.abort(400, "Invalid iso-string")

    homereading = models.HomeReading(
        title=title,
        symbols=symbols,
        reading=reading,
        retelling=retelling,
        translation=translation,
        phrases_or_words=phrases_or_words,
        date=date,
    )

    flask_login.current_user.homereadings.append(homereading)
    models.db.session.commit()

    return "OK"


@api.route("/get-homereadings")
@flask_login.login_required
def api_get_homereadings():
    return flask.jsonify(
        [homereading.get_public_data_as_dict() for homereading in flask_login.current_user.homereadings]
    )


@api.route("/delete-homereading", methods=["POST"])
@flask_login.login_required
def api_delete_homereading():
    """Remove homereading results

    Args:
        homereading_id: int
    Errors:
        400: Bad Request
        403
        404: HR not found
    """

    try:
        homereading_id = int(flask.request.form["homereading_id"])
    except (KeyError, ValueError):
        flask.abort(400)

    homereading = models.HomeReading.query.get(homereading_id)

    if homereading is None:
        flask.abort(404, "HR not found")
    if homereading.user_id != flask_login.current_user.id:
        flask.abort(403)

    models.db.session.delete(homereading)
    models.db.session.commit()

    return "OK"
