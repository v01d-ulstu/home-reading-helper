import datetime

import flask

import api
import config
import login
import models
import views
import cli


app = flask.Flask(__name__, static_folder=config.STATIC_FOLDER)

app.secret_key = config.FLASK_SECRET_KEY
app.config["REMEMBER_COOKIE_DURATION"] = datetime.timedelta(days=config.REMEMBER_COOKIE_DURATION)

app.config["SQLALCHEMY_DATABASE_URI"] = config.DATABASE_FILE
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = config.SQLALCHEMY_TRACK_MODIFICATIONS
models.db.init_app(app)

login.login_manager.init_app(app)
cli.init_cli(app)

app.register_blueprint(api.api, url_prefix="/api/")
app.register_blueprint(views.views)
