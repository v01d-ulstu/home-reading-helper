import flask_sqlalchemy
import flask_login


db = flask_sqlalchemy.SQLAlchemy()


class User(db.Model, flask_login.UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String, unique=True, nullable=False)
    password = db.Column(db.String, nullable=False)
    is_admin = db.Column(db.Boolean, nullable=False, default=False)
    need_symbols = db.Column(db.Integer, nullable=False, default=30_000)

    homereadings = db.relationship("HomeReading", backref="user", lazy=True)

    def get_public_data_as_dict(self):
        return {
            "id": self.id,
            "login": self.login,
            "is_admin": self.is_admin,
            "need_symbols": self.need_symbols,
        }


class HomeReading(db.Model):  # pylint: disable=too-few-public-methods
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), nullable=False)
    symbols = db.Column(db.Integer, nullable=False)
    reading = db.Column(db.Boolean, nullable=False)
    retelling = db.Column(db.Boolean, nullable=False)
    translation = db.Column(db.Boolean, nullable=False)
    phrases_or_words = db.Column(db.Boolean, nullable=False)
    date = db.Column(db.Date, nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)

    def get_public_data_as_dict(self):
        return {
            "id": self.id,
            "title": self.title,
            "symbols": self.symbols,
            "reading": self.reading,
            "retelling": self.retelling,
            "translation": self.translation,
            "phrases_or_words": self.phrases_or_words,
            "date": self.date.isoformat(),  # pylint: disable=no-member
        }
