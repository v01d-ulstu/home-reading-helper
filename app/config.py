import os
import dotenv


dotenv.load_dotenv()


STATIC_FOLDER = os.path.abspath("static")
DATABASE_FILE = "sqlite:///" + os.path.abspath("hrhelper.db")
SQLALCHEMY_TRACK_MODIFICATIONS = False
REMEMBER_COOKIE_DURATION = 365  # days

FLASK_SECRET_KEY = os.environ["FLASK_SECRET_KEY"]

YANDEX_DICT_KEY = os.environ["YANDEX_DICT_KEY"]
YANDEX_DICT_API_URL = "https://dictionary.yandex.net/api/v1/dicservice.json/lookup"
YANDEX_DICT_MAX_TRANSLATION_VARIANTS = 3

MS_TRANSLATOR_KEY = os.environ["MS_TRANSLATOR_KEY"]

MAX_TEXT_LENGTH = 10000
