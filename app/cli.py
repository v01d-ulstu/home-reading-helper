import code

import flask
import click
import werkzeug.security
import sqlalchemy.exc

import models


def init_cli(app: flask.Flask):
    @app.cli.command("init-db")
    def init_db():
        models.db.drop_all()
        models.db.create_all()

        click.echo("OK")

    @app.cli.command("show-users")
    def show_users():
        for user in models.User.query.all():
            print(user.id, user.login)

    @app.cli.command("set-user-password")
    def set_user_password(login, password):
        try:
            user = models.User.query.filter(models.User.login == login).one()
        except sqlalchemy.exc.NoResultFound:
            print(f"User {login} not found")
            return

        user.password = werkzeug.security.generate_password_hash(password)
        models.db.session.commit()

    @app.cli.command("app-context")
    def app_context():
        code.InteractiveConsole(globals()).interact()
