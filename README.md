# ULSTU Home Reading Helper

## Guide

*Это мои методические указания, понимаете?*

### Installing requirements

```ps1
make install-dev
```

### Prepare environ

Set following environment variables:

```.env
FLASK_APP=app/main.py
FLASK_DEBUG=true
YANDEX_DICT_KEY=
MS_TRANSLATOR_KEY=
FLASK_SECRET_KEY=
```

You can use `.env` (see <https://pypi.org/project/python-dotenv/>)

### Lint

```ps1
make lint
```

### DB

Init

```ps1
make init-db
```

Manage models in app context

```ps1
make app-context
```

### Running

```ps1  
flask run
```
