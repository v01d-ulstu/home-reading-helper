run:
	pdm run flask run -p 8040

install:
	pdm install --prod

install-dev:
	pdm install

init-db:
	pdm run flask init-db

show-users:
	pdm run flask show-users

set-user-password:
	pdm run flask set-user-password

app-context:
	pdm run flask app-context

lint:
	pdm run black .
	pdm run pylint app
